-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 23-12-2016 a las 01:12:52
-- Versión del servidor: 5.6.17
-- Versión de PHP: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `adminpro`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_usuario` varchar(200) DEFAULT NULL,
  `usuario_usuario` varchar(200) DEFAULT NULL,
  `pass_usuario` varchar(200) DEFAULT NULL,
  `perfil_usuario` int(11) DEFAULT NULL COMMENT '0 es admin',
  PRIMARY KEY (`id_usuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=61 ;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `nombre_usuario`, `usuario_usuario`, `pass_usuario`, `perfil_usuario`) VALUES
(1, 'osman Ahumada', 'osman', 'admin', 0),
(43, 'xcvxcv', 'xcvxcvx', 'xcvxcv', 0),
(44, 'xcvxcv', 'xcvxcvx', 'xcvxcv', 0),
(45, 'xcvxcv', 'xcvxcvx', 'xcvxcv', 0),
(46, 'xcvxcv', 'xcvxcvx', 'xcvxcv', 0),
(47, 'xcvxcv', 'xcvxcvx', 'xcvxcv', 0),
(54, 'xcvxcv', 'xcvxcvx', 'xcvxcv', 0),
(55, 'xcvxcv', 'xcvxcvx', 'xcvxcv', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
