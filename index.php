<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>tiempo real</title>
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<script src="jquery-2.2.3.min.js"></script>	
	<style>
		body{
			padding-top: 30px;
		}
	</style>
</head>
<body>	
<div class="container">
 <div class="row">

 <div class="col-sm-12 text-center" style="padding-bottom: 50px;">
 	<h2>Ejemplo de crud dinamico con JSON y Jquery</h2>
 </div>
 	<div class="col-sm-5">
 		<form class="form form-horizontal" id="form">
 		 <div class="form-group">
 		 	<label class="control-label col-sm-2">Nombre:</label>
 		 	<div class="col-sm-10">
 		 		<input type="text" class="form-control" name="nombre" placeholder="nombre">
 		 	</div>
 		 </div>
	 	 <div class="form-group">
	 	   <label class="control-label col-sm-2">Usuario:</label>
	 	   <div class="col-sm-10">
	 	   	 <input type="text" class="form-control" name="usuario" placeholder="usuario">
	 	   </div>
	 	 </div>
	 	 <div class="form-group">
	 	 	<label class="control-label col-sm-2">Password</label>
	 	 	<div class="col-sm-10">
	 	 		<input type="text" class="form-control" name="pass" placeholder="pass">
	 	 	</div>
	 	 </div>	 	
	 	 <div class="col-sm-5 col-sm-offset-2">
	 	 	<button class="btn btn-success btn-md form-control" id="guardar">Guardar</button>
	 	 </div>	 	
	    </form>
 	</div>
 	<div class="col-sm-7">
 		<table class="table table-bordered table responsive" > 
 		<thead>
 		  <tr class="text-center">
 		  	<th>ID</th>
 		  	<th>Nombre</th>
 		  	<th>Usuario</th>
 		  	<th>Pass</th>
 		  	<th>Acción</th>
 		  	</tr> 		  	
 		</thead>
 		<tbody id="tabla"> 			
 		</tbody>	
 		</table>
 	</div>
 </div>
</div>
 
<script>


function tabla(data)
{
    var objDatos = eval("(" + data + ")");
	for (var i = 0; i<objDatos.length; i++) 
	{  											
		$('<tr><td class="text-center">'+objDatos[i].id+'</td><td>'+objDatos[i].nombre+'</td><td>'+objDatos[i].usuario+'</td><td>'+objDatos[i].pass+'</td><td><button onClick="eliminar(this.id)" class="btn btn-danger btn-md" id="'+objDatos[i].id+'">eliminar</button></td></tr>').hide().appendTo('#tabla').fadeIn(1000);					
	}	
}
$("#guardar").click(function(){
	$("#tabla").html('');
	var opcion = 'guardar';
	var valores = $( "#form" ).serialize();
	$.ajax({
		url  : 'consulta.php',
		type : 'POST',
		data : valores+"&opcion="+opcion,
		datatype : 'json',
		success : function(data){   
   	  			  tabla(data);	
   	  	}
	});
  return false;
});	
	function eliminar(ide)
   {
   	  $("#tabla").html('');
   	  var id = ide;   	  
   	  var opcion = 'eliminar'; 	     	  
   	  $.ajax({
   	  	url : 'consulta.php',
   	  	type : 'post',
   	  	data : 'id='+id+'&opcion='+opcion,
   	  	datatype : 'json',
   	  	success : function(data){   
   	  			  tabla(data);	
   	  	}
   	  });   
   	
   };
   $.ajax({
		url : 'consulta.php',
		type : 'POST',	
		data : 'opcion=mostrar',
		datatype : 'json',
		success : function(data){
			tabla(data);							
		}
	  });
</script>
</body>
</html>